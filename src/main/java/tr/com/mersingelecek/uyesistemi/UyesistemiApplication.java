package tr.com.mersingelecek.uyesistemi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import tr.com.mersingelecek.uyesistemi.models.Uye;
import tr.com.mersingelecek.uyesistemi.repositories.UyeRepository;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

@SpringBootApplication
public class UyesistemiApplication {


	private static final Logger log = LoggerFactory.getLogger(UyesistemiApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(UyesistemiApplication.class, args);


	}
	public static String telefoonnummerNaarGoedeString(String telefoonnummer) {
		telefoonnummer = telefoonnummer.replaceAll("\\s+", "")
				.replace(".", "-").
				replace("(", "").
				replace(")", "");
		//TODO als het meer dan 10 karakters is, geef aan geen geldige nummer.
		return telefoonnummer;
	}

	@Bean
	public CommandLineRunner demo(UyeRepository repository) throws ParseException {

		LocalDate today = LocalDate.now();
		int dayToday = today.getDayOfMonth();
		int monthNow = today.getMonthValue();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

		return (args) -> {
			for ( Uye uye : repository.findAll()) {
				String geboortedatumString = uye.getDogumtarihi();

				//convert String to LocalDate
				LocalDate geboorteDatum = LocalDate.parse(geboortedatumString, formatter);
				int geboorteDag = geboorteDatum.getDayOfMonth();
				int geboorteMaand = geboorteDatum.getMonthValue();

				if(geboorteDag == dayToday && geboorteMaand == monthNow) {
					String cinsiyeti = uye.getCinsiyet();

					System.out.println(uye.getDogumtarihi() + uye.getCeptelefonu());
//					if(cinsiyeti == "Kadın" ) {
//						cinsiyeti = "hanım";
//					} else {
//						cinsiyeti = "bey";
//					}





					try {
						 String apiUrl = "https://api.vatansms.net/api/v1/1toN";




						String jsonFormData = "{ \"api_id\": \"9d19c177fb9b2521857f5a84\", \"api_key\": \"c9a43232711a6a4c49ec7297\", \"sender\": \"SMS TEST\", \"message_type\": \"normal\", \"message\": \"İyi gunler " + uye.getAdi() + " " + cinsiyeti + ", Gelecek Partisi Mersin ekibi olarak olarak doğum gününüzü en içten dileklerimizle kutlariz.\", \"phones\": [ \""+ telefoonnummerNaarGoedeString(uye.getCeptelefonu())+"\" ] }";

						URL url = new URL(apiUrl);

						HttpURLConnection connect = (HttpURLConnection) url.openConnection();
						connect.setDoOutput(true);
						connect.setConnectTimeout(10000);
						connect.setDoInput(true);
						connect.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
						connect.setRequestMethod("POST");

						OutputStream prepareFormData = connect.getOutputStream();
						prepareFormData.write(jsonFormData.getBytes("UTF-8"));
						prepareFormData.close();

						InputStream inputStream = new BufferedInputStream(connect.getInputStream());
						Scanner s = new Scanner(inputStream).useDelimiter("\\A");
						String result = s.hasNext() ? s.next() : "";

						System.out.println(result);

						inputStream.close();
						connect.disconnect();

					} catch (Exception e) {
						System.out.println("Bir hata ile karşılaşıldı : " + e.getMessage());
					}

				}



			}
		};
	}

}



