package tr.com.mersingelecek.uyesistemi.models;

import javax.persistence.*;

@Entity
@Table(name = "uye")
public class Uye {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "sira")
    private Long id;

    @Column(name = "il")
    private String il;

    @Column(name = "ilce")
    private String ilce;

    @Column(name = "adi")
    private String adi;

    @Column(name = "soyadi")
    private String soyadi;

    @Column(name = "tckimlik")
    private String tckimlik;

    @Column(name = "ceptelefonu")
    private String ceptelefonu;

    @Column(name = "cinsiyet")
    private String cinsiyet;

    @Column(name = "dogumtarihi")
    private String dogumtarihi;

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Uye{" +
                "id=" + id +
                ", il='" + il + '\'' +
                ", ilce='" + ilce + '\'' +
                ", adi='" + adi + '\'' +
                ", soyadi='" + soyadi + '\'' +
                ", tckimlik='" + tckimlik + '\'' +
                ", ceptelefonu='" + ceptelefonu + '\'' +
                ", cinsiyet='" + cinsiyet + '\'' +
                ", dogumtarihi='" + dogumtarihi + '\'' +
                '}';
    }

    public String getIl() {
        return this.il;
    }
    public String getIlce() {
        return this.ilce;
    }
    public String getAdi() {
        return this.adi;
    }
    public String getSoyadi() {
        return this.soyadi;
    }
    public String getTckimlik() {
        return this.tckimlik;
    }
    public String getCeptelefonu() {
        return this.ceptelefonu;
    }
    public String getCinsiyet() {
        return this.cinsiyet;
    }
    public String getDogumtarihi() {
        return this.dogumtarihi;
    }
}
