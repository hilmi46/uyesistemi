package tr.com.mersingelecek.uyesistemi.repositories;

import org.springframework.data.repository.CrudRepository;
import tr.com.mersingelecek.uyesistemi.models.Uye;

import java.util.List;

public interface UyeRepository extends CrudRepository<Uye, Long> {

    List<Uye> findBySoyadi(String soyadi);

    Uye findUyeById(Long id);
}
